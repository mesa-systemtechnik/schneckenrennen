import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.13

Window {
    visible: true
    width: 1280
    height: 1024
    title: "Schneckenrennen"

    Connections {
        target: cube
    }




    Grid {
        id: grid
        x: 40
        y: 84
        width: 1200
        height: 700
        rows: 6
        columns: 9

        Rectangle {
            id: start_0
            width: 130
            height: 116
            color: "#f40f0f"
            radius: 5
        }

        Rectangle {
            id: field_0_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_0_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_0_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_0_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_0_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_0_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_0_6
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: end_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: start_1
            width: 130
            height: 116
            color: "#2f1df4"
            radius: 5
            border.width: 0
        }

        Rectangle {
            id: field_1_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_1_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_1_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_1_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_1_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_1_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_1_6
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: end_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: start_2
            width: 130
            height: 116
            color: "#efed29"
            radius: 5
            border.width: 0
        }

        Rectangle {
            id: field_2_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_2_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_2_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_2_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_2_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_2_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_2_6
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: end_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: start_3
            width: 130
            height: 116
            color: "#b20ed8"
            radius: 5
            border.width: 0
        }

        Rectangle {
            id: field_3_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_3_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_3_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_3_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_3_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_3_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_3_6
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: end_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: start_4
            width: 130
            height: 116
            color: "#59f528"
            radius: 5
            border.width: 0
        }

        Rectangle {
            id: field_4_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_4_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_4_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_4_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_4_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_4_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_4_6
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: end_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: start_5
            width: 130
            height: 116
            color: "#fab911"
            radius: 5
            border.width: 0
        }

        Rectangle {
            id: field_5_0
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_5_1
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_5_2
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_5_3
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_5_4
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_5_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: field_5_6
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }

        Rectangle {
            id: end_5
            width: 130
            height: 116
            color: "#ffffff"
            radius: 5
            border.width: 3
        }


    }

    Image {
        id: slug_0
        x: 55
        y: 95
        width: 100
        height: 100
        fillMode: Image.PreserveAspectFit
        source: "slug.png"
    }

    Image {
        id: slug_1
        x: 55
        y: 208
        width: 100
        height: 100
        source: "slug.png"
        fillMode: Image.PreserveAspectFit
    }


    Image {
        id: slug_2
        x: 56
        y: 323
        width: 100
        height: 100
        source: "slug.png"
        fillMode: Image.PreserveAspectFit
    }



    Image {
        id: slug_3
        x: 56
        y: 439
        width: 100
        height: 100
        source: "slug.png"
        fillMode: Image.PreserveAspectFit
    }

    Image {
        id: slug_4
        x: 56
        y: 555
        width: 100
        height: 100
        source: "slug.png"
        fillMode: Image.PreserveAspectFit
    }

    Image {
        id: slug_5
        x: 56
        y: 671
        width: 100
        height: 100
        source: "slug.png"
        fillMode: Image.PreserveAspectFit
    }


    Text {
        id: title
        x: 506
        y: 23
        text: qsTr("Schneckenrennen")
        font.bold: true
        font.pixelSize: 30
    }

    Button {
        id: play_button
        x: 40
        y: 857
        width: 191
        height: 109
        text: qsTr("Play")
        font.pointSize: 20

        onClicked: {
            var color_number = cube.roll_the_dice();

            switch (color_number)
            {
            case 1:
                cube_color.color = '#f40f0f';
                animation_0.target = slug_0
                animation_0.to += 130
                animation_0.start()
                break;
            case 2:
                cube_color.color = '#2f1df4';
                animation_1.target = slug_1
                animation_1.to += 130
                animation_1.start()
                break;
            case 3:
                cube_color.color = '#efed29';
                animation_2.target = slug_2
                animation_2.to += 130
                animation_2.start()
                break;
            case 4:
                cube_color.color = '#b20ed8';
                animation_3.target = slug_3
                animation_3.to += 130
                animation_3.start()
                break;
            case 5:
                cube_color.color = '#59f528';
                animation_4.target = slug_4
                animation_4.to += 130
                animation_4.start()
                break;
            case 6:
                cube_color.color = '#fab911';
                animation_5.target = slug_5
                animation_5.to += 130
                animation_5.start()
                break;
            }



        }

    }

    NumberAnimation {
        id: animation_0
        target: slug_0
        running: false
        property: "x"
        to: 55
        duration: 1000
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: animation_1
        target: slug_1
        running: false
        property: "x"
        to: 55
        duration: 1000
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: animation_2
        target: slug_2
        running: false
        property: "x"
        to: 55
        duration: 1000
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: animation_3
        target: slug_3
        running: false
        property: "x"
        to: 55
        duration: 1000
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: animation_4
        target: slug_4
        running: false
        property: "x"
        to: 55
        duration: 1000
        easing.type: Easing.InOutQuad
    }

    NumberAnimation {
        id: animation_5
        target: slug_5
        running: false
        property: "x"
        to: 55
        duration: 1000
        easing.type: Easing.InOutQuad
    }

    Rectangle {
        id: cube_background
        x: 1089
        y: 857
        width: 122
        height: 116
        color: "#ffffff"
        radius: 21
        border.width: 3

    }

    Rectangle {
        id: cube_color
        x: 1108
        y: 874
        width: 84
        height: 76
        color: "#ffffff"
        radius: 5
    }

    Text {
        id: element
        x: 257
        y: 874
        text: qsTr("Würfel:")
        font.pixelSize: 16
    }

    Text {
        id: element1
        x: 257
        y: 899
        text: qsTr("Gewinner:")
        font.pixelSize: 16
    }

    Text {
        id: element2
        x: 257
        y: 924
        text: qsTr("Gewinner:")
        font.pixelSize: 16
    }

    Button {
        id: button
        x: 907
        y: 861
        width: 165
        height: 109
        text: qsTr("Würfeln")
        font.pointSize: 16
    }

}
