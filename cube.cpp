#include "cube.h"
#include <QDateTime>
#include <iostream>

using namespace std;

Cube::Cube(QObject *parent) : QObject(parent) { m_color = 0; }

int Cube::roll_the_dice()
{
  srand(QDateTime::currentMSecsSinceEpoch());
  int random = (rand() % 6) + 1;

  cout << "Zufallszahl: " << random << endl;

  m_color = random;

  return random;
}
