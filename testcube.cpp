#include "testcube.h"

void TestCube::toUpper()
{
  QString str = "Hello";
  QVERIFY(str.toUpper() == "HELLO");
}

void TestCube::test_roll_the_dice_give_a_number()
{
  Cube cube;
  QVERIFY(cube.roll_the_dice() != NULL);
}

QTEST_MAIN(TestCube)
