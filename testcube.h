#ifndef TESTCUBE_H
#define TESTCUBE_H

#include "cube.h"
#include <QObject>
#include <QtTest/QtTest>

class TestCube : public QObject
{
  Q_OBJECT
private slots:
  void test_toUpper();
  void test_roll_the_dice_is_not_null();
};

#endif
// TESTCUBE_H
