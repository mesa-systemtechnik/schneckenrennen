#ifndef CUBE_H
#define CUBE_H
#pragma once

#include <QObject>

class Cube : public QObject
{
  Q_OBJECT

private:
  unsigned int m_color;

public slots:
  int roll_the_dice();

public:
  explicit Cube(QObject *parent = nullptr);
};

#endif // CUBE_H
